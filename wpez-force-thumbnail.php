<?php
/**
 * Plugin Name: WPezPlugins: Force Thumbnail
 * Plugin URI: https://gitlab.com/WPezPlugins/wpez-force-thumbnail
 * Description: If a post has no thumbnail, an image is automatically from a hierarchy of possible options.
 * Version: 0.0.1
 * Author: Mark F. Simchock (Chief Executive Alchemist @ Alchemy United)
 * Author URI: https://AlchemyUnited.com/utm_source=wpez_ft
 * Text Domain: wpez_ft
 * License: GPLv2 or later
 *
 * @package WPezForceThumbnail
 */

namespace WPezForceThumbnail;

defined( 'ABSPATH' ) || exit;

// As seen here: https://developer.wordpress.org/reference/functions/get_post_thumbnail_id/ .
add_filter( 'post_thumbnail_id', __NAMESPACE__ . '\postThumbnailID', PHP_INT_MAX - 1000, 2 );

// Take the thumb_id as provided by the earlier priority of the filter and maybe set it as the post thumbnail.
add_filter( 'post_thumbnail_id', __NAMESPACE__ . '\setPostThumbnail', PHP_INT_MAX - 500, 2 );

// https://developer.wordpress.org/reference/hooks/wp_after_insert_post/ .
add_action( 'wp_after_insert_post', __NAMESPACE__ . '\wpAfterInsertPost', 100000, 4 );

/**
 * For this WP core action: do_action( 'wp_after_insert_post', $post_id, $post, $update, $post_before ); .
 *
 * @param int      $post_id - Post ID.
 * @param \WP_Post $post - A WP post object.
 * @param bool     $update - Whether this is an existing post being updated or not.
 * @param mixed    $post_before - The value of $post before the update.
 *
 * @return bool|integer
 */
function wpAfterInsertPost( int $post_id, \WP_Post $post, bool $update, $post_before ) {

	// If it's a new post then don't do anything.
	if ( ! $update ) {
		return -1;
	}

	$arr_blacklist = apply_filters( __NAMESPACE__ . '\post_type_blacklist', array() );
	if ( ! is_array( $arr_blacklist ) ) {
		$arr_blacklist = array();
	}

	// TODO - also add whitelist?
	if ( 'revision' === $post->post_type || in_array( $post->post_type, $arr_blacklist, true ) ) {
		return -2;
	}

	$int_thumb_id = 0;
	$int_thumb_id = postThumbnailID( $int_thumb_id, $post );

	if ( false !== $int_thumb_id && 0 !== $int_thumb_id ) {

		return setPostThumbnail( $int_thumb_id, $post );
	}
	return -3;
}

/**
 * Our own version of has_post_thumbnail() without the filters (so we don't get stuck in an infinite loop).
 *
 * @param \WP_Post $post - a WP post object
 *
 * @return bool
 */
function hasPostThumbnail( \WP_Post $post ) {

	$thumbnail_id  = (int) get_post_meta( $post->ID, '_thumbnail_id', true );
	$has_thumbnail = (bool) $thumbnail_id;

	return $has_thumbnail;
}

/**
 * For this WP core filter: apply_filters( 'post_thumbnail_id', $thumb_id, $post_id ); .
 *
 * @param int      $int_thumb_id - The post thumbnail ID.
 * @param \WP_Post $post - A WP post object.
 *
 * @return int|false
 */
function postThumbnailID( int $int_thumb_id, \WP_Post $post ) {

	if ( 0 !== $int_thumb_id ) {
		return $int_thumb_id;
	}

	$arr_blacklist = apply_filters( __NAMESPACE__ . '\post_type_blacklist', array() );
	if ( ! is_array( $arr_blacklist ) ) {
		$arr_blacklist = array();
	}

	// TODO - also add whitelist?
	if ( in_array( $post->post_type, $arr_blacklist, true ) ) {
		return false;
	}

	// If the post type supports thumbnails, and there's no thumbnail, then we'll try to find one.
	if ( ! hasPostThumbnail( $post ) && checkThemeSupport( $post->post_type ) ) {

		$int_thumb_id = 0;
		// Try to find an image in the post content?
		if ( true === apply_filters( __NAMESPACE__ . '\use_first_content_img_flag', false, $int_thumb_id, $post ) ) {

			$the_content = $post->post_content;
			// TODO - do all this with regex.
			// TODO - check the found img for size and orientation.
			$str_start = '<!-- wp:image ';
			$str_end   = '<!-- /wp:image -->';
			$int_start = strpos( $the_content, $str_start );
			$int_end   = strpos( $the_content, $str_end );

			// Did we find what we were looking for?
			if ( false !== $int_start && false !== $int_end ) {

				$str_first_image = substr( $the_content, $int_start + strlen( $str_start ), $int_end - ( $int_start + strlen( $str_start ) ) );
				// now get the json encoded string
				$int_end_2 = strpos( $str_first_image, ' -->' );
				if ( false !== $int_end_2 ) {
					$str_first_image_json = substr( $str_first_image, 0, $int_end_2 );

					$arr_first_image = json_decode( $str_first_image_json, true );
					if ( isset( $arr_first_image['id'] ) ) {
						$int_thumb_id = (int) $arr_first_image['id'];
					}
				}
			}
		}

		// Did we find a thumbnail? If not, look by tax > term.
		if ( 0 === $int_thumb_id && true === apply_filters( __NAMESPACE__ . '\use_post_type_tax_flag', false, $int_thumb_id, $post ) ) {

			// return a associative array of pairs: cat_id => img_id.
			// TODO - build an admin UI to enter these?
			$arr_post_type_tax_term_img_ids = apply_filters( __NAMESPACE__ . '\post_type_tax_term_img_ids', array(), $post );

			if ( is_array( $arr_post_type_tax_term_img_ids ) && ! empty( $arr_post_type_tax_term_img_ids )
			&& isset( $arr_post_type_tax_term_img_ids[ $post->post_type ] ) && is_array( $arr_post_type_tax_term_img_ids[ $post->post_type] ) ) {

				$str_tax = key( $arr_post_type_tax_term_img_ids[ $post->post_type ] );

				// Get all the cats assigned to the post.
				$arr_terms = get_the_terms( $post, $str_tax );

				if ( is_array( $arr_terms ) ) {
					foreach ( $arr_terms as $wp_term ) {
						if ( isset( $arr_post_type_tax_term_img_ids[ $post->post_type ][ $str_tax ][ $wp_term->term_id ] ) ) {
							$int_thumb_id = (int) $arr_post_type_tax_term_img_ids[ $post->post_type ][ $str_tax ][ $wp_term->term_id ];
							// We found a thumbnail, so we're done.
							break;
						}
					}
				}
			}
		}

		// Still no thumbnail? How about an img for the post_type?
		if ( 0 === $int_thumb_id ) {

			$int_thumb_id_post_type = apply_filters( __NAMESPACE__ . '\post_type_img_id', 0, $post );

			if ( 0 !== $int_thumb_id_post_type ) {
				$int_thumb_id = $int_thumb_id_post_type;
			}
		}

		// Did we find a thumbnail? If not, look for the last chance image (aka global default).
		if ( 0 === $int_thumb_id ) {

			// Return the img id of the default (aka last chance) img. 
			// TODO - build an admin UI to enter this?
			$int_last_chance_img_id = apply_filters( __NAMESPACE__ . '\last_chance_img_id', 0, $post );

			// PJR - Same as above. Hardcoded this to test in my local env.
			// you'll need to change this to match your site, or remove it and use the filter above.

			if ( 0 !== $int_last_chance_img_id ) {
				$int_thumb_id = (int) $int_last_chance_img_id;
			}
		}
	}
	return $int_thumb_id;
}

/**
 * A custom version of set_post_thumbnail().
 *
 * @param integer $int_thumb_id - Thumbnail ID.
 * @param \WP_Post $post - A WP post object.
 *
 * @return int|bool
 */
function setPostThumbnail( int $int_thumb_id, \WP_Post $post ) {

	// Do we want to (retroactively) set_post_thumbnail()? default is no (false). Else, we'll just use the $int_thumb_id on the fly
	$bool = apply_filters( __NAMESPACE__ . '\set_post_thumbnail_flag', false, $int_thumb_id, $post );

	if ( true === $bool ) {
		// Does the post have a thumbnail already?
		$int_thumb_id_orig = (int) get_post_meta( $post->ID, '_thumbnail_id', true );

		// If not, then set it.
		if ( empty( $int_thumb_id_orig ) && 0 !== $int_thumb_id ) {

			$int_thumb_id = set_post_thumbnail( $post, $int_thumb_id );
		}
	}
	return $int_thumb_id;
}

/**
 * Checks if the post_type supports thumbnails.
 *
 * @param string $post_type The post type.
 *
 * @return boolean
 */
function checkThemeSupport( string $post_type ) {

	global $_wp_theme_features;

	$post_type = strtolower( trim( $post_type ) );

	if ( isset( $_wp_theme_features['post-thumbnails'] ) ) {

		// add_theme_support() for 'post-thumbnails' was set without specifying a post type (so if it's set, it's all post_types).
		if ( true === $_wp_theme_features['post-thumbnails'] ) {
			return true;
		}

		// add_theme_support() for 'post-thumbnails' can be an array of post types.
		if ( isset( $_wp_theme_features['post-thumbnails'][0] ) && is_array( $_wp_theme_features['post-thumbnails'][0] ) ) {

			if ( in_array( $post_type, $_wp_theme_features['post-thumbnails'][0], true ) ) {
				return true;
			}
		}
	}
	return false;
}
