# Plugin Name: WPezPlugins: Force Thumbnail
### A WordPress Plugin

Description: If a post / page/ etc. has no thumbnail, an image (ID) can automatically be assigned based on a hierarchy of filter-based options.

The primary WP core filter used is 'post_thumbnail_id' which was introduced in WP 5.9.0.

https://developer.wordpress.org/reference/hooks/post_thumbnail_id/


There is also a sibling plugin that provides a user-friendly WP Admin UI for configuring the "settings" of this plugin. That UI plugin has a series for add_filter()s which passes back the UI-maintained WP option values to this plugin's various apply_filters()s.

The README for the sibling UI plugin has details on the filters / settings. 

https://gitlab.com/WPezPlugins/wpez-force-thumbnail-ui




